'''@file        hw0x02.py
@brief          Simulates an elevator
@details        Implements a finite state machine, shown below, to simulate
                the behavior of an elevator operating between 2 floors with
                a motor and operation buttons
@author         Tim Jain
@date           1/27/21
@copyright      License Info Here
'''

import time
import random


def motor_cmd(cmd):
    '''@brief Commands the motor to move or stop
       @details The motor runs forward when assigned a 1 (elevator moves up),
       in reverse when assigned a 2 (elevator moves down), and stops
       when assigned a 0 (elevator stops)
       @param cmd The command to give the motor
    '''
    if cmd== 1:
        print('Fwd: Moving up')
    elif cmd==2:
        print('Rev: Moving down')
    elif cmd==0:
        print('Motor Stopped')

def button_1():
    ''' @brief 1st floor button
        @details uses random number generator module to choose a value for
        true or false indicating true for when the button is pressed for the
        first floor, and false when the button is not pressed. Within 
        the main script we see a few things: When this function is activated while at the first floor, it will stay at the 
        first floor until button_2() is pressed. Else, it will activate the 
        motor to enable movement upwards between floors
        @param empty 
        @return True or False random selection
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_2():
    ''' @brief 2nd floor button
        @details uses random number generator module to choose a value for
        true or false indicating true for when the button is pressed for the
        second floor, and false when the button is not pressed. Within 
        the main script we see a few things: When this function is activated while at the second floor, it will stay at the 
        second floor until button_1() is pressed. Else, it will activate the 
        motor to enable movement downwards between floors
        @param empty 
        @return True or False random selection
    '''
    return random.choice([True, False]) # randomly returns T or F

def floor_1():
    ''' @brief 1st floor indication
        @details uses random number generator module to choose a value for
        true or false to indicate whether floor 1 is reached or not
        @param empty 
        @return True or False random selection
    '''
    return random.choice([True, False]) # randomly returns T or F

def floor_2():
    ''' @brief 2nd floor indication
        @details uses random number generator module to choose a value for
        true or false to indicate whether floor 2 is reached or not
        @param empty 
        @return True or False random selection
    '''
    return random.choice([True, False]) # randomly returns T or F

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    
    while True:
        try:
            # main program code goes here
            if state==0:
                # run state 0 (init) code
                print('S0: Moving Down')
                # When the floor_1 button is enabled, the motor will stop,
                # and the button_1 will be disabled, indicating that we are
                # at state 1
                if floor_1():
                    motor_cmd(0) # Command motor to go forward
                    button_1()
                    state = 1   # Updating state for next iteration
                
            elif state==1:
                # run state 1 (stopped on floor 1) code
                print('S1: Stopped on Floor 1')
                # If we are stopped at Floor 1, start the motor and transition 
                # to S2. 
                # If we enable the button_1 function and let it be empty, we 
                # will allow the elevator to stay on the first floor if the
                # button is not pressed until button_2 is pressed, in which 
                # case the motor will run forward and will move up to state
                # floor 2
                if button_1():
                    button_1()
                elif button_2():
                    motor_cmd(1)
                    state = 2
                    
            elif state==2:
                # run state 2 (moving up) code
                print('S2:Moving Up')
                # if floor_2 button is enablen, then upon a true 
                # paramater, there will be an indication that floor 2 has
                # been reached, stopping the motor and turning off the 
                # button_2, reaching state 3
                if floor_2():
                    motor_cmd(0)
                    button_2()
                    state = 3
                
            elif state==3:
                # run state 3 (stopped at floor 2) code
                print('S3: Stopped at Floor 2')
                # If the button_2 function is enabled while we are already at
                # state 2, the elevator will do nothing until button_1 is 
                # pressed, and then the motor is set to rev, moving the 
                # elevator down (state 0)
                if button_2():
                    button_2()
                elif button_1():
                    motor_cmd(2)
                    state= 0
                    
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
            # Slow down execution of FSM so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break

    # Program de-initialization goes  here